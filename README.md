# convo-platform

Small nodejs package that **should** (*hopefully*) make building chatbots easier.

### Installation

This is a [Node.js](https://nodejs.org/en/) module available through the
[npm registry](https://www.npmjs.com/).

Before installing, [download and install Node.js](https://nodejs.org/en/download/).
Node.js v8 or higher is required.

Install the package using the [`npm install` command](https://docs.npmjs.com/getting-started/installing-npm-packages-locally):

```bash
$ npm install convo-platform
```


### Features

* State definition
* State import/export


### Documentation

WIP


### Quick Start

WIP


### Examples

WIP